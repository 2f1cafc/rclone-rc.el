;;; emacs library that talks to rclone's remote control interface:
;;;     - https://rclone.org/rc/

(require 'json)
(require 'subr-x)

(defconst rclone-rc/--version "0.1")

;;; TODO -> send password if authentication is required
(defvar rclone-rc/use-authentication nil
  "If non-nil, then we will need to send a username and password when accessing certain restrcited rclone endpoints (and rclone should be configured as such).

If nil, then we will send '--rc-no-auth' when starting rc in order to bypass it.")

(defvar rclone-rc/use-loopback t
  "If true then rclone will service the request directly and then
  exit. Otherwise, we need to connect to a separate, long running
  instance of the server (which could also be remote) which will have its own lifecycle and
  each invocation of rclone will talk to it for one command.")

(defvar rclone-rc/rclone-command "rclone"
  "command to run to execute rclone")

(defvar rclone-rc/do-command-hooks nil
  "Hooks to run prior to executing a command. Functions will be
  called with a string representing the command this about to be
  run.")

(defvar rclone-rc/auth-source-spec '(:host "rclone")
  "Pattern to use to find the rclone config password in auth-sources, if neccessary. (See rclone-rc/maybe-set-config-password-from-auth-sources)")

(defun rclone-rc/make-command (name &rest params)
  (seq-filter
   'identity
   (list
	rclone-rc/rclone-command
	"-q"
	"rc"
	(unless rclone-rc/use-authentication "--rc-no-auth")
	(if rclone-rc/use-loopback "--loopback")
	(and params
		 (concat "--json "
				 (shell-quote-argument
				  (json-encode params))))
	(if (symbolp name)
		(symbol-name name) name))))

(defun rclone-rc/do-command (cmd &optional response-key)
  (run-hooks 'rclone-rc/do-command-hooks)
  (let*
	  ((json-array-type 'list)
	   (json
		(with-temp-buffer
		  (if (not
			   (zerop
				(apply 'call-process
					   (car cmd) nil
					   (current-buffer) nil
					   (cdr cmd))))
			  (error (buffer-string))
			(goto-char (point-min))
			(json-read)))))
	;; perhaps pull out specific key from the response
	(if response-key
		(cdr (assoc response-key json))
	  ;; otherwise just return the result as is
	  json)))

(defun rclone-rc/--cmd-noop ()
  (rclone-rc/do-command
   (rclone-rc/make-command 'rc/noop)))

(defun rclone-rc/--cmd-version ()
  (rclone-rc/do-command
   (rclone-rc/make-command 'core/version)))

(defun rclone-rc/--cmd-listremotes ()
  "Query rclone and return a sequence of remotes."
  (rclone-rc/do-command
   (rclone-rc/make-command 'config/listremotes)
   'remotes))

(defun rclone-rc/--cmd-list (fs remote)
  "List a path, returning an array of its contents."
  (rclone-rc/do-command
   (rclone-rc/make-command 'operations/list
						   :fs fs
						   :remote remote)))

(defun rclone-rc/maybe-set-config-password (password)
  "Do nothing if there is already a config password in the
  environment. Otherwise, if password is a string, set the
  password to it. If it is a function, call it and set the
  password to the result."

  (unless (getenv "RCLONE_CONFIG_PASS")
	(setenv "RCLONE_CONFIG_PASS"
			(cond
			 ((functionp password) (funcall password))
			 ((stringp password) password)))))

(defun rclone-rc/maybe-set-config-password-from-auth-sources ()
  "Looks up the password in auth-sources if neccessary."
  (rclone-rc/maybe-set-config-password
   (lambda ()
	 (funcall (plist-get
			   (car (apply #'auth-source-search rclone-rc/auth-source-spec)) :secret)))))

(add-hook 'rclone-rc/do-command-hooks 'rclone-rc/maybe-set-config-password-from-auth-sources)

;; (rclone-rc/maybe-set-config-password
;;  (lambda () (password-store-get "rclone")))

;;; (rclone-rc/listremotes)

;; (vectorp (cdr (assoc 'remotes

;; (rclone-rc/do-command
;;  (rclone-rc/make-command 'rc/noop '(param1 . "foo")))

;; (provide 'rclone-rc)

;; (insert (shell-quote-argument (json-encode '(("ac". 1)))))\{\"ac\"\:1\}

(defun rclone-rc/version ()
  "Ask rclone for it's version. If called interactively, will
show the result from rclone as well as the version of this
library in the minibuffer. Will throw an error if it cannot
communicate with rclone."
  (interactive)
  (let* ((j (rclone-rc/--cmd-version))
		 (v (alist-get 'version j)))
	(if (called-interactively-p 'any)
		(message "rclone version: %s, rclone-rc.el version: %s"
				 v rclone-rc/--version)
	  v)))

;;  TODO: cache listremotes calls (and others ...?)
;; (setq rclone-rc/--remotes-cache nil
;; 	  "Cache of last list of remotes.")

(defun rclone-rc/list-remotes ()
  (interactive)
  "List remotes, possibly from cache."
  (rclone-rc/--cmd-listremotes))

(defun rclone-rc/read-remote-completing ()
  "Will read a remote from the user, with completion."
  (completing-read "Remote: " (rclone-rc/list-remotes)))
